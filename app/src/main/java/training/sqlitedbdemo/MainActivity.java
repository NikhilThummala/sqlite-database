package training.sqlitedbdemo;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import training.sqlitedbdemo.database.Database;

public class MainActivity extends AppCompatActivity {

    Button insert, view;

    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        insert = (Button) findViewById(R.id.button);
        view = (Button) findViewById(R.id.button2);

        db = new Database(getApplicationContext());
        db.openDatabase();

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InsertActivity.class));
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = db.getAllValues();
                if(cursor.getCount() > 0) {
                    startActivity(new Intent(getApplicationContext(), ViewActivity.class));
                } else {
                    Toast.makeText(getApplicationContext(), "No rows present!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
       // db.closeDatabase();
    }
}
