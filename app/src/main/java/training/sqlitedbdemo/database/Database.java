package training.sqlitedbdemo.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database {

    private static final String DB_NAME = "Details";
    private static final int DB_VERSION = 1;

    private static final String TABLE_NAME = "DetailsTable";
    private static final String COL_ROW_ID = "rowId";
    private static final String COL_NAME = "name";
    private static final String COL_EMAIL_ID = "email";

    private static final String CREATE_TABLE = "create table DetailsTable(rowId integer primary key autoincrement, name text not null, " +
            "email text not null)";

    private static final String CREATE_TABLE_1 = "create table " + TABLE_NAME + "(" + COL_ROW_ID + " integer primary key autoincrement, " +
            COL_NAME + " text not null, " + COL_EMAIL_ID + " text not null)";


    DBHelper dbHelper;
    SQLiteDatabase db;

    public Database(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void openDatabase() {
        db = dbHelper.getWritableDatabase();
    }

    public void closeDatabase() {
        db.close();
    }

    public void insertValues(String name, String emailId) {
        ContentValues con = new ContentValues();
        con.put(COL_NAME, name);
        con.put(COL_EMAIL_ID, emailId);
        db.insert(TABLE_NAME, null, con);
    }

    public void updateValues(String rowId, String updatedName, String updatedEmail){
        ContentValues con = new ContentValues();
        con.put(COL_NAME, updatedName);
        con.put(COL_EMAIL_ID, updatedEmail);
        db.update(TABLE_NAME, con, rowId + "=" + COL_ROW_ID, null);
    }

    public void deleteRow(String rowId) {
        db.delete(TABLE_NAME, rowId + "=" + COL_ROW_ID, null);
    }

    public void deleteAllRows() {
        db.delete(TABLE_NAME, null, null);
    }

    public Cursor getAllValues() {
        //Cursor cursor = db.rawQuery("select * from DetailTable", null);
        String[] columns = {COL_ROW_ID, COL_NAME, COL_EMAIL_ID};
        Cursor cursor = db.query(TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }


    private class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
