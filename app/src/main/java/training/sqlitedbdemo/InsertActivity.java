package training.sqlitedbdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import training.sqlitedbdemo.database.Database;

public class InsertActivity extends AppCompatActivity {

    EditText name, email;
    Button insert, cancel;

    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        name = (EditText) findViewById(R.id.editText);
        email = (EditText) findViewById(R.id.editText2);
        insert = (Button) findViewById(R.id.button3);
        cancel = (Button) findViewById(R.id.button4);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = new Database(getApplicationContext());
                db.openDatabase();

                db.insertValues(name.getText().toString(), email.getText().toString());

                Toast.makeText(getApplicationContext(), "Values inserted", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.closeDatabase();
    }
}
