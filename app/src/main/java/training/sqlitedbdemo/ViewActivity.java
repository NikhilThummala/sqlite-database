package training.sqlitedbdemo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import training.sqlitedbdemo.database.Database;

public class ViewActivity extends AppCompatActivity {

    ListView listView;

    Database db;
    Cursor cursor;
    int pos;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        listView = (ListView) findViewById(R.id.listview);

        db = new Database(getApplicationContext());
        db.openDatabase();

        cursor = db.getAllValues();

        adapter = new CustomAdapter();
        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                pos = position;
                return false;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.contextmenu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.update) {
            final Dialog dialog = new Dialog(ViewActivity.this);
            dialog.setContentView(R.layout.update);
            dialog.show();

            final EditText updatedName = (EditText) dialog.findViewById(R.id.editText3);
            final EditText updatedEmail = (EditText) dialog.findViewById(R.id.editText4);
            Button update = (Button) dialog.findViewById(R.id.button5);

            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cursor.moveToPosition(pos);
                    db.updateValues(cursor.getString(0), updatedName.getText().toString(), updatedEmail.getText().toString());
                    Toast.makeText(getApplicationContext(), "Values updated", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    cursor = db.getAllValues();
                    adapter.notifyDataSetChanged();
                }
            });

        } else if (item.getItemId() == R.id.delete) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ViewActivity.this);
            builder.setTitle("Delete");
            builder.setMessage("Are you sure to delete?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    cursor.moveToPosition(pos);
                    db.deleteRow(cursor.getString(0));
                    Toast.makeText(getApplicationContext(), "Values deleted", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    cursor = db.getAllValues();
                    adapter.notifyDataSetChanged();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionsmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.deleteall) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ViewActivity.this);
            builder.setTitle("Delete all");
            builder.setMessage("Are you sure to delete all?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    db.deleteAllRows();
                    Toast.makeText(getApplicationContext(), "Values deleted", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    cursor = db.getAllValues();
                    adapter.notifyDataSetChanged();
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    public class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return cursor.getCount();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            view = getLayoutInflater().inflate(R.layout.custom_layout, null);
            TextView row = (TextView) view.findViewById(R.id.textView);
            TextView name = (TextView) view.findViewById(R.id.textView2);
            TextView emailId = (TextView) view.findViewById(R.id.textView3);

            cursor.moveToPosition(position);
            row.setText(cursor.getString(0));
            name.setText(cursor.getString(1));
            emailId.setText(cursor.getString(2));

            return view;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.closeDatabase();
    }
}
